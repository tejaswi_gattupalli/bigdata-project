# Python library imports: numpy, random, sklearn, pandas, etc

import sys
import random
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.cluster import KMeans
import pydoop.hdfs as hdfs
import matplotlib.cm as cm
from random import shuffle
def ApplyKmeans(X, n_clusters):  
  kmeans = KMeans(n_clusters=n_clusters).fit(X)
  centers = (kmeans.cluster_centers_)
  print("Clustered the dataset")
  colors = cm.rainbow(np.linspace(0, 1, n_clusters))
  X_clusterwise = []
  for i in range(n_clusters):
    X_clusterwise.append([])
  for li,xi in zip(kmeans.labels_,X):
    X_clusterwise[li].append(xi)
  for i,xc in enumerate(X_clusterwise):
    Xs, Ys = [],[]
    for xi,yi in xc:
      Xs.append(xi)
      Ys.append(yi)
    plt.scatter(Xs, Ys, s=5, color=colors[i], cmap="coolwarm")
  plt.scatter(centers[:,0], centers[:,1], s=5, marker='X', c='black')
  plt.title(str(n_clusters)+' clusters/zones for crimes in Chicago')
  plt.xlabel('LATITUDE')
  plt.ylabel('LONGITUDE')
  plt.show()

  rel_freqs = [50, 100]
  colors = np.r_[rel_freqs] 
  mymap = plt.get_cmap("Reds")
  my_colors = mymap(colors)
  avg_freq = float(len(X)/float(n_clusters))
  X_high, Y_high = [],[]
  X_low, Y_low = [],[]
  for xc in (X_clusterwise):
    Xs, Ys = [],[]
    for xi,yi in xc:
      Xs.append(xi)
      Ys.append(yi)
    if len(xc) > avg_freq:    
      for xi,yi in xc:
        X_high.append(xi)
        Y_high.append(yi)
    else:    
      for xi,yi in xc:
        X_low.append(xi)
        Y_low.append(yi)
  plt.scatter(X_high, Y_high, color=my_colors[1], s=5, edgecolors='None', label="High frequency zone")
  plt.scatter(X_low, Y_low, color=my_colors[0], s=5, edgecolors='None', label="Low frequency zone")
    
  plt.scatter(centers[:,0], centers[:,1], s=5, marker='X', c='black')
  plt.legend(scatterpoints=1)
  plt.title(str(n_clusters)+' clusters/zones based on crime frequencies in Chicago')
  plt.xlabel('LATITUDE')
  plt.ylabel('LONGITUDE')
  plt.show()

def PlotDensity(df, X, col_headings, id):

  tags = df[col_headings[id]]
  tag_name = col_headings[id]
  print(tag_name)
  counts = df[col_headings[id]].value_counts().as_matrix()[:5]
  vals = df[col_headings[id]].value_counts().index.tolist()[:5]
  y_pos = np.arange(len(vals))
  
  plt.bar(y_pos, counts, align='center', alpha=0.5)
  plt.xticks(y_pos, vals)#, rotation='vertical')
  plt.ylabel("Number of occurences")
  plt.title('Frequency of values of '+tag_name)
  
  plt.show()
  
  unique_vals = list(set(tags))
  #print(unique_vals)
  counts_tag={}
  counts = []
  for val in unique_vals:
    counts_tag[val]=0
  #print(counts_tag)
  for tag in tags:
    counts_tag[tag]+=1
  for val in unique_vals:
    counts.append(counts_tag[val])
  rel_freqs = [counts_tag[val]/1000 for val in unique_vals]
  X_clusterwise = []
  indices_tag={}
  for i in range(len(unique_vals)):
    indices_tag[unique_vals[i]]=i
  for i in range(len(unique_vals)):
    X_clusterwise.append([])
  for li,xi in zip(tags,X):
    X_clusterwise[indices_tag[li]].append(xi)
  colors = np.r_[rel_freqs] 
  mymap = plt.get_cmap("Blues")
  my_colors = mymap(colors)
  
  colors = cm.rainbow(np.linspace(0, 1, 5))

  sorted_counts = sorted(counts)[::-1]
  if len(unique_vals) > 5:
    max_5_count = sorted_counts[5]
  j=0
  for i,xc in enumerate(X_clusterwise):
    Xs, Ys = [],[]
    for xi,yi in xc:
      Xs.append(xi)
      Ys.append(yi)
    #if (len(unique_vals)>5 and counts_tag[unique_vals[i]] > max_5_count) or len(unique_vals)<5:
    #  plt.scatter(Xs, Ys, s=10,color=my_colors[i], edgecolors='None',label=unique_vals[i])
    if (len(unique_vals)>5 and counts_tag[unique_vals[i]] > max_5_count) or len(unique_vals)<5:
      plt.scatter(Xs, Ys, s=2,color=colors[j], cmap="coolwarm",label=unique_vals[i])
      j+=1
    #else:
    #  plt.scatter(Xs, Ys, s=10,color=my_colors[i], edgecolors='None')
  plt.legend(scatterpoints=1)
  plt.title('Based on '+str(tag_name))
  plt.xlabel('LATITUDE')
  plt.ylabel('LONGITUDE')
  plt.show()
  
fn = sys.argv[1]
print("HDFS File path: {}".format(hdfs.ls(fn)))
with hdfs.open(fn) as f:
  df = pd.read_csv(f)
print("Read unformatted dataset from HDFS")
col_headings = list(df)
new_df = df[col_headings[-3:-1]]
dataset = new_df.as_matrix()
X = []
for row in dataset:
  if not(np.isnan(row[0]) or np.isnan(row[1])):
    X.append([row[0], row[1]])  
n_clusters = int(raw_input("Number of clusters: "))
ApplyKmeans(X, n_clusters)

str_cols_ids = range(7)
str_cols_ids.append(9)
print("List of Attributes:\nId\tAttribute")
for id in str_cols_ids:
  print("{}\t{}".format(id,col_headings[id]))
id = int(raw_input("Apply density plot on which attribute(id): "))
PlotDensity(df, X, col_headings, id)