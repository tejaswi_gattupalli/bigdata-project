# Python library imports: numpy, random, sklearn, pandas, etc
import sys
import random
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.cluster import KMeans
import pydoop.hdfs as hdfs
import matplotlib.cm as cm
from random import shuffle
from hdfs import InsecureClient
import os

def SplitDataset(dataset, fn, sr):
    trainfn = fn[:-4]+"_train.csv"
    testfn = fn[:-4]+"_test.csv"
    print(trainfn)
    print(testfn)

    client_hdfs = InsecureClient('http://localhost:9870')

    train_data = []
    test_data = []
    no_samples = int(sr*len(dataset))
    print(no_samples)

    prev_ids = []
    for i in range(no_samples):
        curr_id = np.random.randint(len(dataset))
        if len(prev_ids) > 0:
            while curr_id in prev_ids:
                curr_id = np.random.randint(len(dataset))
        prev_ids.append(curr_id)
        train_data.append(dataset[curr_id])

    for i in range(len(dataset)):
        if i not in prev_ids:
            test_data.append(dataset[i])

    train_df = pd.DataFrame(train_data)
    with client_hdfs.write(trainfn, encoding = 'utf-8') as writer:
        train_df.to_csv(writer)
    test_df = pd.DataFrame(test_data)
    with client_hdfs.write(testfn, encoding = 'utf-8') as writer:
        test_df.to_csv(writer)
    
fn = sys.argv[1]
sr = float(raw_input("Enter sampling ratio(train/test): "))
print(fn)
print(hdfs.ls(fn))
i=0
with hdfs.open(fn) as f:
  df = pd.read_csv(f)
dataset = df.as_matrix()
print(dataset)
SplitDataset(dataset, fn, sr)