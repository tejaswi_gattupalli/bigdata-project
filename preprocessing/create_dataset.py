# Python library imports: numpy, random, sklearn, pandas, etc
import sys
import random
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.cluster import KMeans
import pydoop.hdfs as hdfs
import matplotlib.cm as cm
from random import shuffle
from hdfs import InsecureClient
import os

def Plot_freqs(X, n_clusters, X_clusterwise, centers):
    #PLOTTING CLUSTER BY FREQS    
    rel_freqs = [50, 100]
    colors = np.r_[rel_freqs] 
    mymap = plt.get_cmap("Reds")
    my_colors = mymap(colors)
    avg_freq = float(len(X)/float(n_clusters))
    X_high, Y_high = [],[]
    X_low, Y_low = [],[]
    for xc in (X_clusterwise):
        Xs, Ys = [],[]
        print(xc[0])
        for row in xc:
            xi,yi = row[-2:]
            Xs.append(xi)
            Ys.append(yi)
        if len(xc) > avg_freq:
            for row in xc:
                xi,yi = row[-2:]
                X_high.append(xi)
                Y_high.append(yi)
        else:    
            for row in xc:
                xi,yi = row[-2:]
                X_low.append(xi)
                Y_low.append(yi)
    plt.scatter(X_high, Y_high, color=my_colors[1], s=5, edgecolors='None', label="High frequency zone")
    plt.scatter(X_low, Y_low, color=my_colors[0], s=5, edgecolors='None', label="Low frequency zone")
        
    plt.scatter(centers[:,0], centers[:,1], s=5, marker='X', c='black')
    plt.legend(scatterpoints=1)
    plt.title(str(n_clusters)+' clusters/zones based on crime frequencies in Chicago')
    plt.show()
    #PLOTTING CLUSTER BY FREQS


def CreateDataset(X, n_clusters, df, outputfn):
    col_headings = list(df)
    new_df = df[col_headings[2:-1]]
    print("Filtered columns, new dataframe: {}".format(list(new_df)))
    
    X_old = []
    dataset = new_df.as_matrix()
    str_cols_ids = range(7)
    str_cols_ids.append(9)

    for row in dataset:
        flag=1
        for i in range(len(row)):
            if i not in str_cols_ids and np.isnan(row[i]):
                flag=0
                break
        if flag==1:
            X_old.append(row)
    print("Filtered out missing values")
    col_headings = list(new_df)
    unique_vals_per_col = {}
    for i in str_cols_ids:
        curr_col = df[col_headings[i]]
        curr_len=len(set(curr_col))
        unique_vals_per_col[i]=curr_len
    
    str_maps_li = {}
    for i in str_cols_ids:
        str_maps_li[i]={}
    X1=[]
    col_maxs = {}
    for i in str_cols_ids:
        col_maxs[i]=0
    for row in X_old:
        curr_row=[]
        for i in range(len(row)):
            if i in str_cols_ids:
                if row[i] not in str_maps_li[i]:
                    str_maps_li[i][row[i]]=col_maxs[i]
                    col_maxs[i]+=1
                curr_row.append(str_maps_li[i][row[i]])
            else:
                curr_row.append(row[i])
        X1.append(curr_row)
    print("Mapped strings to integers in dataset")
    
    X=[]
    for row in X1:
        X.append(row[-2:])
    kmeans = KMeans(n_clusters=n_clusters).fit(X)
    centers = (kmeans.cluster_centers_)
    print("Clustered the dataset")
    
    X_clusterwise = []
    for i in range(n_clusters):
        X_clusterwise.append([])
    for li,xi in zip(kmeans.labels_,X1):
        X_clusterwise[li].append(xi)
    
    #Plot_freqs(X, n_clusters, X_clusterwise, centers)
    
    cluster_freqs=[]
    print("Grouped dataset into clusters")
    #Compute frequency for each cluster in the region
    for xc in (X_clusterwise):
        cluster_freqs.append(len(xc))
    
    #Compute average frequency of crimes for the whole region
    avg_crime_freq = float(len(X)/float(n_clusters))
    X_new = []
    Y = []
    for i,xc in enumerate(X_clusterwise):
        #Categorize as high or low frequency zone
        if cluster_freqs[i] > avg_crime_freq:
            curr_class = 1
        else:
            curr_class = 0
        for xi in xc:
            X_new.append(xi)
            Y.append(curr_class)
    print("Categorized clusters based on crime frequency")
    #Merge and shuffle dataset
    dataset = []
    for xi,yi in zip(X_new, Y):
        curr_row = [ele for ele in xi]
        curr_row.append(yi)
        dataset.append(curr_row)
    
    shuffle(dataset)
    
    df = pd.DataFrame(dataset)
    print("Created dataset")
    
    client_hdfs = InsecureClient('http://localhost:9870')
    
    with client_hdfs.write(outputfn, encoding = 'utf-8') as writer:
        df.to_csv(writer)
    print("Wrote formatted dataset to HDFS")

fn = sys.argv[1]
outputfn = sys.argv[2]
#print(fn)
print("HDFS File path: {}".format(hdfs.ls(fn)))
with hdfs.open(fn) as f:
  df = pd.read_csv(f)
print("Read unformatted dataset from HDFS")
col_headings = list(df)
new_df = df[col_headings[-3:-1]]
dataset = new_df.as_matrix()
X = []
for row in dataset:
  if not(np.isnan(row[0]) or np.isnan(row[1])):
    X.append([row[0], row[1]])  
n_clusters = int(raw_input("Number of clusters: "))


CreateDataset(X, n_clusters, df, outputfn)