# Python library imports: numpy, random, sklearn, pandas, etc
import sys
import random
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.cluster import KMeans
import pydoop.hdfs as hdfs
import matplotlib.cm as cm
from random import shuffle
from hdfs import InsecureClient
import os
from sklearn.cluster import KMeans
from math import exp
from scipy.linalg import pinv
from scipy.interpolate import spline
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import tensorflow as tf

def onehotvector(x):
    y = []
    for xi in x:
        if(xi == 0):
            y.append([1, 0])
        else:
            y.append([0, 1])
    return np.array(y)    

def argmax(x):
    for i in range(len(x)):
        if(x[i] == 1):
            return i

def Dist(p1, p2):
    s = 0
    for i in range(len(p1)):
        s += (p1[i]-p2[i])**2
    return (s**0.5) 
  
class rbfn:
    
    def __init__(self, A, B):
        self.A = A
        self.B = B
        return

    
    def calcG(self, X):
        G = np.zeros((self.K, X.shape[0]))
        for i in range(self.K):
            for j in range(X.shape[0]):
                G[i][j] = exp(-self.B * Dist(X[j], self.V[i]))
        return G
    
    def Fit(self, X, d, d_original, K):
        self.K = K
        km = KMeans(n_clusters = self.K, init='random', n_init=5, max_iter=100).fit(X)
        self.V = km.cluster_centers_
        G = self.calcG(X).T
        P = pinv(np.dot(G.T, G))
        Q = np.dot(P, G.T)
        self.w = np.dot(Q, d)
        print("Input layer Dimensions: {}".format(X.shape[1]))
        print("RBFN layer Dimensions: {}".format(self.K))
        print("Output layer Dimensions: {}".format(d.shape[1]))
        print("Weight Dimensions: {} * {} ".format(self.w.shape[0], self.w.shape[1]))
        return
    
    def Predict(self, x):
        G = np.zeros((self.K))
        for i in range(self.K):
            G[i] = exp(-self.B * Dist(x, self.V[i]))
        
        y = np.dot(G, self.w)
        for i in range(len(y)):
            if(y[i] == max(y)):
                return i
    
    def predict(self, X):
        Y = []
        for xi in X:
            Y.append(self.Predict(xi))
        return np.array(Y)
        
    def Decision_boundary(self, X, y):        
        rel_freqs = [50, 100]
        colors = np.r_[rel_freqs] 
        mymap = plt.get_cmap("Reds")
        my_colors = mymap(colors)
        X_high, Y_high = [],[]
        X_low, Y_low = [],[]
        for xi,yi in zip(X,y):
            if yi==1:
                X_high.append(xi[0])
                Y_high.append(xi[1])
            else:
                X_low.append(xi[0])
                Y_low.append(xi[1])

        plt.scatter(X_high, Y_high, color=my_colors[1], edgecolors='None', label="High frequency zone")
        plt.scatter(X_low, Y_low, color=my_colors[0], edgecolors='None', label="Low frequency zone")
            
        #plt.scatter(centers[:,0], centers[:,1], marker='X', c='black')
        plt.legend(scatterpoints=1)
        plt.title('Hotspot zone prediction by RBFN')
        plt.xlabel('LATITUDE')
        plt.ylabel('LONGITUDE')
        plt.show()

        '''
        h = .02 
        x_min, x_max = X[:, 0].min()-0.1, X[:, 0].max()+0.1
        y_min, y_max = X[:, 1].min()-0.1, X[:, 1].max()+0.1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        title = 'Hotspot zone prediction by RBFN'
        Z = self.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        #plt.contourf(xx, yy, Z, s=5, cmap=plt.cm.coolwarm, alpha=0.8)
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
        plt.xlabel('LATITUDE')
        plt.ylabel('LONGITUDE')
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.xticks(()),plt.yticks(())
        plt.title(title)
        plt.legend(scatterpoints=1)
        plt.show()
        return
        '''

def ReadDataset(fn):
    with hdfs.open(fn) as f:
        df = pd.read_csv(f)
    dataset = df.as_matrix()
    dataset_new = []
    for row in dataset:
        flag=1
        for ele in row:
            if np.isnan(ele):
                flag=0
                break
        if flag==1:
            dataset_new.append(row[-3:])
    return dataset_new

def Splitdata(dataset):
    X,Y=[],[]
    for row in dataset:
        curr_x = row[:-1]
        curr_y = row[-1]
        X.append(curr_x)
        Y.append(curr_y)
    return [np.array(X),np.array(Y)]

def Merge(d1, d2, t1, t2):
    d = []
    for di in d1:
        d.append(di)
    for di in d2:
        d.append(di)
    t = []
    for di in t1:
        t.append(di)
    for di in t2:
        t.append(di)
    num_rows = len(t)
    return [np.array(d),np.array(t)]

fn = sys.argv[1]
print(fn)
print(hdfs.ls(fn))

trainfn = fn[:-4]+"_train.csv"
testfn = fn[:-4]+"_test.csv"


train_dataset = ReadDataset(trainfn)
test_dataset = ReadDataset(testfn)

train_X, train_Y = Splitdata(train_dataset)
test_X, test_Y = Splitdata(test_dataset)

full_X, full_Y = Merge(train_X, test_X, train_Y, test_Y)


train_target = onehotvector(train_Y)
test_target = onehotvector(test_Y)


r1 = rbfn(1500, 1)

r1.Fit(train_X, train_target, train_Y, 50)    

test_pred = r1.predict(test_X)

train_pred = r1.predict(train_X)

acc = 0
for yi,di in zip(train_pred, train_Y):
    if yi==di:
        acc+=1
print("RBFN Train Accuracy: {:.2f}%".format(float((acc*100.0)/len(train_Y))))


acc = 0
tp,tn,fp,fn=0,0,0,0
for yi,di in zip(test_pred, test_Y):
    if yi==di:
        acc+=1
    if di==1:
        if yi==di:
            tp+=1
        else:
            fn+=1
    else:
        if yi==di:
            tn+=1
        else:
            fp+=1
print("RBFN Test Accuracy: {:.2f}%".format(float((acc*100.0)/len(test_Y))))
prec = float(tp/float(tp+fp))
rec = float(tp/float(tp+fn))
f1 = (2*prec*rec)/float(prec+rec)
print("Conf matrix")
print("{}\t{}".format(tp,fp))
print("{}\t{}".format(fn,tn))
print("Precision: {:.2f}".format(prec))
print("Recall: {:.2f}".format(rec))
print("F1: {:.2f}".format(f1))

r1.Decision_boundary(full_X, full_Y)
'''
clf = DecisionTreeClassifier(
    max_features=0.6,
    min_samples_split=0.2,
    min_samples_leaf=0.2
)
clf.fit(train_X, train_Y) 
test_pred = clf.predict(test_X)

acc = 0
for yi,di in zip(test_pred, test_Y):
    if yi==di:
        acc+=1

print("Accuracy: {:.2f}%".format(float((acc*100.0)/len(test_Y))))

       
x = tf.placeholder(tf.float32, [None, 16])
w1 = tf.Variable(tf.zeros([16, 100]))
b1 = tf.Variable(tf.zeros([100]))
y1 = tf.nn.sigmoid(tf.matmul(x, w1) + b1)

w2 = tf.Variable(tf.zeros([100, 100]))
b2 = tf.Variable(tf.zeros([100]))
y2 = tf.nn.sigmoid(tf.matmul(y1, w2) + b2)
             
w3 = tf.Variable(tf.zeros([100, 2]))
b3 = tf.Variable(tf.zeros([2]))
y = tf.matmul(y2, w3) + b3

             
d = tf.placeholder(tf.float32, [None, 2])

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels = d, logits = y))

#train_step = tf.train.MomentumOptimizer(0.1, 0.9).minimize(cross_entropy)
train_step = tf.train.AdagradOptimizer(0.5).minimize(cross_entropy)

correct_predictions = tf.equal(tf.arg_max(y, 1), tf.arg_max(d, 1))

accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))

sess = tf.InteractiveSession()

init = tf.global_variables_initializer()
sess.run(init)


fn = sys.argv[1]
print(fn)
print(hdfs.ls(fn))

trainfn = fn[:-4]+"_train.csv"
testfn = fn[:-4]+"_test.csv"


train_dataset = ReadDataset(trainfn)
test_dataset = ReadDataset(testfn)

train_X, train_Y = Splitdata(train_dataset)
test_X, test_Y = Splitdata(test_dataset)

train_target = onehotvector(train_Y)
test_target = onehotvector(test_Y)

for i in range(40):
    print("Epoch :{}\tCross entropy: {:.8f}".format(i, sess.run(cross_entropy, {x: train_X, d: train_target})))
    sess.run(train_step, {x: train_X, d: train_target})


print("\nTesting data\nPredicted: {} \nExpected:  {}".format(sess.run(tf.arg_max(y, 1), {x: test_X, d: test_target}), sess.run(tf.arg_max(d, 1), {d: test_target})))
print("Training accuracy: {}%".format(sess.run(accuracy*100, {x: train_X, d: train_target})))
print("Testing accuracy: {:.2f}%".format(sess.run(accuracy*100, {x: test_X, d: test_target})))
'''